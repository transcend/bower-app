# App Module
The "App" module provides common components that are used in every app as well as skeleton components to help
bootstrap an angular app quickly.

## Installation
The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
commands to install this package:

```
bower install https://bitbucket.org/transcend/bower-app.git
```

Other options:

*   * Download the code at [http://code.tsstools.com/bower-app/get/master.zip](http://code.tsstools.com/bower-app/get/master.zip)
*   * View the repository at [http://code.tsstools.com/bower-app](http://code.tsstools.com/bower-app)
*   * If you have permissions, view the source code at [http://code.tsstools.com/web-common](http://code.tsstools.com/web-common/src/ee541a3bce850ae77f6d7948a00cc352b4f67b21/modules/app/?at=develop)

When you have pulled down the code and included the files (and dependency files), simply add the module as a
dependency to your application:

```
angular.module('myModule', ['transcend.app']);
```

## To Do
- Pull in additional components from existing apps.

## Project Goals
- Keep this module size below 15kb minified