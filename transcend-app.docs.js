/**
   * @ngdoc overview
   * @name transcend.app
   * @description
   # App Module
   The "App" module provides common components that are used in every app as well as skeleton components to help
   bootstrap an angular app quickly.

   ## Installation
   The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
   commands to install this package:

   ```
   bower install https://bitbucket.org/transcend/bower-app.git
   ```

   Other options:

   *   * Download the code at [http://code.tsstools.com/bower-app/get/master.zip](http://code.tsstools.com/bower-app/get/master.zip)
   *   * View the repository at [http://code.tsstools.com/bower-app](http://code.tsstools.com/bower-app)
   *   * If you have permissions, view the source code at [http://code.tsstools.com/web-common](http://code.tsstools.com/web-common/src/ee541a3bce850ae77f6d7948a00cc352b4f67b21/modules/app/?at=develop)

   When you have pulled down the code and included the files (and dependency files), simply add the module as a
   dependency to your application:

   ```
   angular.module('myModule', ['transcend.app']);
   ```

   ## To Do
   - Pull in additional components from existing apps.

   ## Project Goals
   - Keep this module size below 15kb minified
   */
/**
   * @ngdoc object
   * @name transcend.app.appConfig
   *
   * @description
   * Provides a single configuration object that can be used to change the behaviour, options, urls, etc for any
   * components in the {@link transcend.app App Module}. The "appConfig" object is a
   * {@link https://docs.angularjs.org/api/ng/type/angular.Module module constant} and can therefore be
   * overridden/configured by:
   *
   <pre>
   // Override the value of the 'appConfig' object.
   angular.module('myApp').value('appConfig', {
      profile: {
        id: 'tss'
      },
      serviceUrl: 'http://localhost/tds',
      version: '__version__',
      buildDate: '__buildDate__',
      description: '__description__',
      usersGuide: 'docs',
      serviceApi: '../../help',
      codeDocumentation: 'code',
      modal: {
        size: 'lg'
      }
   });
   </pre>
   **/
/**
 * @ngdoc controller
 * @name transcend.app.controller:AboutCtrl
 *
 * @description
 * The 'AboutCtrl' controller provides control for about pages. The purpose of the about page is to simply display
 * basic information about the current application. Things like, build date, description, version, etc.
 *
 * @requires transcend.app.appConfig
 *
 * @example
 <example module="app">
 <file name="index.html">
 <div ng-include="'web-common/modules/app/about/about.html'"></div>
 </file>
 <file name="script.js">
 angular.module('app', ['transcend.app'])
 .config(function(appConfig){
  appConfig.version = '1.0.0';
  appConfig.buildDate = 'Monday, December 14th, 2015, 9:57:29 PM';
  appConfig.description = ' Transcend is a geospatial information technology (IT) company specializing in transportation. We have experience leveraging leading commercial GIS and database products and datasets to develop custom applications to meet specific business processes. We also have developed COTS solutions that can be used to jumpstart transportation projects resulting in more stable and cost effective implementations. The Transcend team has worked with many state and local government agencies, as well as private firms, to provide geospatial services. Our depth of experience allows us to deliver performance beyond expectations.';
 });
 </file>
 </example>
 */
/**
 * @ngdoc controller
 * @name transcend.app.controller:AccountDialogCtrl
 *
 * @description
 * The 'AccountDialogCtrl' controller handles  account dialog menu options, and creates the 'My Info' tab, the
 * 'Change Password' tab, and the 'My Privileges' tab.
 *
 * @requires $scope
 * @requires $modalInstance
 * @requires transcend.security.$authenticate
 */
/**
 * @ngdoc controller
 * @name transcend.app.controller:AccountRegistrationDialogCtrl
 *
 * @description
 * The 'AccountRegistrationDialogCtrl' controller controls the Account Registration Dialog menu.
 *
 * @requires $loading
 * @requires $scope
 * @requires $modalInstance
 *
 */
/**
 * @ngdoc controller
 * @name transcend.app.controller:ChangePasswordCtrl
 *
 * @description
 * The 'ChangePasswordCtrl' controller controls the Account Registration Dialog menu.
 *
 * @requires $scope
 * @requires transcend.core.$notify
 * @requires transcend.security.service:Account
 *
 */
/**
 * @ngdoc controller
 * @name transcend.app.controller:ResetPasswordCtrl
 *
 * @description
 * The 'ResetPasswordCtrl' controller controls the Reset Password dialog.
 *
 * @requires $scope
 * @requires transcend.core.$notify
 * @requires transcend.security.service:Account
 * @requires $modalInstance
 *
 */
/**
 * @ngdoc controller
 * @name transcend.app.controller:RolesInfoCtrl
 *
 * @description
 * The 'RolesInfoCtrl' controller has information about the roles for users, and counts the number of Users that have
 * a specific role.
 *
 * @requires $scope
 * @requires transcend.security.$roleManager
 * @requires $Privilege
 *
 */
/**
 * @ngdoc controller
 * @name transcend.app.controller:UserInfoCtrl
 *
 * @description
 * The 'UserInfoCtrl' controller handles updating and changing information/permissions in User Accounts
 *
 * @requires $scope
 * @requires $window
 * @requires transcend.security.$authenticate
 * @requires User
 * @requires transcend.core.$notify
 *
 */
/**
 * @ngdoc directive
 * @name transcend.app.directive:dateRangePicker
 *
 * @description
 * The 'dateRangePicker' directive provides the ability to create a datePicker that allows the user to pick
 * both a From Date and a To Date.
 *
 * @restrict EA
 * @scope
 *
 * @requires transcend.app.appConfig
 *
 * @example
 <example module="app">
 <file name="index.html">
 <div ng-controller="Ctrl as ctrl">
 <date-range-picker from-date="ctrl.from" to-date="ctrl.to"></date-range-picker>
 </div>
 </file>
 <file name="script.js">
 angular.module('app', ['transcend.app'])
 .controller('Ctrl', function() {
    this.from = new Date();
    this.to = new Date();
});
 </file>
 </example>
 **/
/**
 * @ngdoc directive
 * @name transcend.app.directive:floatingPane
 *
 * @description
 * The 'floatingPane' directive creates a floating window pane, which is a fixed container that can sit at any corner
 * of the screen and house any elements. The floating pane will automatically change to "relative" positioning when
 * the screen size becomes too small.
 *
 * <pre>
 <floating-pane position-class="bottom-left">
 <!-- TODO: Bookmarks Menu -->
 </floating-pane>
 * </pre>
 *
 * @restrict EA
 * @scope
 * @requires transcend.app.appConfig
 */
/**
 * @ngdoc directive
 * @name transcend.app.directive:dbConnectionsMenu
 *
 * @description
 * The 'dbConnectionsMenu' directive creates a simple navigation menu item to show the "db connections manager"
 * component.
 *
 * <pre>
 <li class="dropdown" dropdown>
   <a href class="dropdown-toggle" dropdown-toggle>More <b class="caret"></b></a>
   <ul class="dropdown-menu" role="menu">
     <li db-connections-menu></li>
   </ul>
 </li>
 * </pre>
 *
 * @restrict EA
 * @scope
 * @requires transcend.app.service:$showModal
 * @requires transcend.app.appConfig
 *
 * @example
 <example module="app">
 <file name="index.html">
 <div ng-controller="Ctrl as ctrl">
 <nav class="navbar navbar-default" role="navigation">
   <span class="navbar-brand" >Nav Bar</span>
   <ul class="nav navbar-nav main-nav nav-pills">
     <li class="dropdown" dropdown>
      <a href class="dropdown-toggle" dropdown-toggle><i class="glyphicon glyphicon-plus"></i> More <b class="caret"></b></a>
      <ul class="dropdown-menu" role="menu">
        <li><a href role="menuitem">Menu Item 1</a></li>
        <li db-connections-menu></li>
        <li><a href role="menuitem">Menu Item 1</a></li>
       </ul>
     </li>
  </ul>
 </nav>

 </div>
 </file>
 <file name="script.js">
 angular.module('app', ['transcend.app', 'ngMockE2E'])
 .controller('Ctrl', function() {
  });
 </file>
 <file name="mocks.js">
 angular.module('app')
 .run(function($httpBackend, $roleManager){
   $roleManager.enabled = false;

   var mocks = [{id: 'DefaultConnection', name: 'Default Connection', provider: 'System.Data.SqlServerCe.4.0', connectionString:'Data Source=|DataDirectory|\TDS.sdf'},
              {id: 'MySqlDb', name: 'Sql Server DB', provider: 'System.Data.SqlClient', connectionString:'data source=sql.myhost.com, 1433;initial catalog=MY_CATALOG;persist security info=True;user id=suer;password=password'},
              {id: 'MyOracleDb', name: 'Oracle DB', provider: 'System.Data.OracleClient', connectionString:'Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=oracle.myhost.com)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=MY_SERVICE)));User Id=user;Password=password;'}];

   // Mock our HTTP calls.
   $httpBackend.whenGET('/api/data/source')
    .respond(angular.copy(mocks));

   angular.forEach(mocks, function(mock){
     $httpBackend.whenGET('/api/data/source/' + mock.id)
      .respond(angular.copy(mock));

     $httpBackend.whenPOST('/api/data/source/test/' + mock.id)
      .respond({"passed":true,"message":"Connection was successful","provider":mock.provider});
   });
 })
 </file>
 </example>
 **/
/**
 * @ngdoc controller
 * @name transcend.app.controller:ErrorReportCtrl
 *
 * @description
 * The 'ErrorReportCtrl' controller holds the logic for the error reporting menu, which sends an email with the details
 * written by the user who experienced the error.
 *
 * @requires $scope
 * @requires $log
 * @requires $modalInstance
 * @requires transcend.core.$string
 * @requires transcend.security.$authenticate
 * @requires transcend.app.appConfig
 * @requires $loading
 * @requires transcend.core.$notify
 * @requires Email
 **/
/**
 * @ngdoc directive
 * @name transcend.app.directive:helpMenu
 *
 * @description
 * The 'helpMenu' directive provides a starting template for help Menus.
 *
 * @restrict EA
 * @element ANY
 * @scope
 * @requires transcend.app.service:$showModal
 * @requires transcend.app.appConfig
 *
 * @example
 <example module="app">
 <file name="index.html">
 <div ng-controller="Ctrl as ctrl">
 <nav class="navbar navbar-default" role="navigation">
 <span class="navbar-brand" >Nav Bar</span>
 <ul class="nav navbar-nav main-nav nav-pills">
 <li help-menu></li>
 </ul>
 </nav>

 </div>
 </file>
 <file name="script.js">
 angular.module('app', ['transcend.app', 'ngMockE2E'])
 .controller('Ctrl', function() {
  });
 </file>
 <file name="mocks.js">
 angular.module('app')
 .run(function(appConfig, $roleManager, $log){
   var log = $log.start('Configuring application').last;
   $roleManager.enabled = false;
   appConfig.buildDate = new Date();
   appConfig.version = '1.0.0';
   appConfig.description = 'Some description of the application';
   $log.stop(log);

   $log.warn('This is a warning');
   $log.error('This is an error');
   $log.debug('This is a debug');
 })
 </file>
 </example>
 **/
/**
 * @ngdoc service
 * @name transcend.app.provider:$app
 *
 * @description
 * The '$app' factory provides functionality to bootstrap (initialize) and application. The intent is that the app
 * will load a "profile" (configuration entity) either from a REST service or from a JSON file.
 *
 * <pre>
 // Configure the application load/module process.
 $appProvider
 .configure({
          app: {
            appId: 'my-app-id',
            serviceUrl: 'http://localhost/tds'
          },
          autoConfigs: [
            'tsConfig',
            'esriConfig',
            'lrsConfig',
            'securityConfig',
            'securityManagerConfig',
            'reportingConfig',
            'lidarConfig',
            'dataSourceConfig',
            'drawingConfig',
            'exportConfig',
            'jobConfig'
          ]
        })
 .waitsFor(myCustomInitializationStep)
 .hooks({configureModules: myConfigureModulesHook, resolve: myOnResolveHook});
 * </pre>
 *
 * @requires transcend.app.appConfig
 *
 * @param {object|string=} config A configuration object that represents the "appConfig" or a string which represents
 * the "appId".
 */
/**
       * @ngdoc method
       * @name transcend.app.provider:$app#config
       * @propertyOf transcend.app.provider:$app
       * @description
       * Configuration object on the provider that dictates behaviour and exposes hooks/functionality.
       * <pre>
       $appProvider.config = {
            autoConfigs: [],
            hooks: {
              beforeSignIn: noop,
              afterSignIn: noop,
              beforeProfileLoad: noop,
              afterProfileLoad: noop,
              configureModules: noop,
              resolve: noop,
              onRejected: noop
            },
            defaultProfileFlag: 'default'
          };
       * </pre>
       */
/**
 * @ngdoc directive
 * @name transcend.app.directive:loaded
 *
 * @description
 * The 'loaded' directive provides a the ability to bind (a hook) to whether an element loads or not. This is typically
 * used to keep the window loading screen shown until the theme (css file) has fully loaded - but it is not limited
 * to that scenario or CSS files.
 *
 * <pre>
 <link rel="stylesheet" loaded="app.themeLoaded" ng-if="app.hasTheme" ng-href="themes/{{theme}}.css" />
 * </pre>
 *
 * @scope
 *
 * @param {boolean=} loaded A flag that represents when the item has loaded - either successfully or failed.
 * @param {boolean=} loadedSuccess A flag that represents when the item has loaded successfully.
 * @param {boolean=} loadedFailed A flag that represents when the item failed to load.
 */
/**
 * @ngdoc controller
 * @name transcend.app.controller:LogCtrl
 *
 * @description
 * The 'LogCtrl' controller holds the logic for using the logging functions.
 *
 * @requires $scope
 * @requires $log

 *
 *
 */
/**
 * @ngdoc service
 * @name transcend.app.service:$showModal
 *
 * @description
 * The '$showModal' service provides a quick way to pop open a bootstrap modal, using default configuration. It also
 * provides a more flexible manner of passing configuration to the modal. In addition, if you omit a controller
 * parameter it will use a default controller function with the 'ok', 'close', and 'cancel' methods available on
 * the scope.
 *
 <pre>
 $showModal('web-common/modules/my-view.html', 'MyCtrl', true);

 $showModal('web-common/modules/my-view.html');

 $showModal({
   templateUrl: 'web-common/modules/my-view.html',
   controller: 'MyCtrl',
   backdrop: true,
   keyboard: false
 });
 </pre>
 *
 * @requires $modal
 * @requires transcend.app.appConfig
 *
 * @param {string|object} template The URL to the template or the complete configuration object.
 * @param {string|function=} controller The controller name of function.
 * @param {boolean=} strict Whether to allow clicking in the modal backdrop to close it or not (strict mode).
 **/
/**
 * @ngdoc directive
 * @name transcend.app.directive:resizeModal
 *
 * @restrict EA
 * @description
 * The 'resizeModal' directive allows the modal window to have a toggleable full screen option. The component is
 * typically used within the 'modal-header' portion of a modal.
 *
 * <pre>
 <div class="modal-header text-center">
  <resize-modal close="close()"></resize-modal>
  <h1>About</h1>
 </div>

 <div class="modal-body"></div>
 <div class="modal-footer"></div>
 * </pre>
 *
 * @requires $timeout
 *
 * @param {string=} state The default state to start at. Valid values are: '' or 'maximized'.
 * @param {string=} maximized Flag determining whether the modal size should be maximized or not.
 * @param {function=} close Expression fired when "close" button is clicked.
 * @param {function=} onResize Expression fired when the modal state is changed/toggled.
 **/
/**
 * @ngdoc service
 * @name transcend.app.directive:appProfileList
 *
 * @description
 * Provides a simple list of profiles and provides the ability to add/delete them - if you have the privileges to.
 *
 * @requires $window
 * @requires $location
 * @requires transcend.app#appConfig
 * @requires transcend.app#AppProfile
 * @requires transcend.core#$notify
 * @requires transcend.core#$array
 *
 * @param {string=} appId The ID of the "App" profiles that you want to show. This will default to appConfig.appId.
 *
 * @example
 <example module="app">
 <file name="index.html">
 <div ng-controller="Ctrl as ctrl">
 <app-profile-list add-roles="1" delete-roles="2"></app-profile-list>

 <h1>Or Place in a Modal:</h1>
 <button class="btn btn-primary btn-lg" ng-click="ctrl.modal('web-common/modules/app/profile/app-profile-list-dialog.html'">
 Show Profile List in Modal
 </button>
 </div>
 </file>
 <file name="script.js">
 angular.module('app', ['transcend.app', 'ngMockE2E'])
 .run(function($roleManager) {
    $roleManager.roles([1,2]);
 })

 .controller('Ctrl', function(appConfig, $showModal) {
    this.modal = $showModal;

    appConfig.appName = 'My App';
    appConfig.appId = 'my-app'
    appConfig.profile = {
      id: 'tss'
    };
  });
 </file>
 <file name="mocks.js">
 angular.module('app')
 .run(function($httpBackend){
   var mocks = [{"homePageUrl":"http://www.coloradodot.info/","errorReportEmail":"","bypassProxy":true,"signSystems":[],"links":[],"esriConfig":{"basemapSelector":{},"mapServer":{"url":"//dev-app/arcgis/rest/services/cdot/lrs/MapServer","hasLrsMeta":false,"hasNumericRouteId":false},"arcgis":{}},"id":"cdot","name":"Colorado Road Analyzer","logo":"images/logos/cdot-logo.png","appId":"ra","theme":"midnight"},{"homePageUrl":"http://ddot.dc.gov/","message":"<strong>Welcome!</strong> If this is your first time viewing Road Analyzer check out our <a href=\"docs/#/user\" target=\"_blank\">Users Guide</a> for help navigating the application.","errorReportEmail":"","bypassProxy":true,"signSystems":[],"links":[],"settings":{"id":4,"scale":50.0,"visibility":{}},"esriConfig":{"basemapSelector":{},"mapServer":{"url":"//dev-app/arcgis/rest/services/ddot/lrs/MapServer","hasLrsMeta":false,"hasNumericRouteId":false},"arcgis":{}},"id":"ddot","name":"DDOT Road Analyzer","logo":"images/logos/ddot-logo.png","appId":"ra","theme":"patriot"},{"homePageUrl":"http://www.dot.ga.gov/Pages/default.aspx","errorReportEmail":"","bypassProxy":true,"signSystems":[],"links":[],"esriConfig":{"basemapSelector":{},"mapServer":{"url":"//dev-app/arcgis/rest/services/gdot/lrs/MapServer","hasLrsMeta":false,"hasNumericRouteId":false},"arcgis":{}},"id":"gdot","name":"Georgia Road Analyzer","logo":"images/logos/gdot-logo.png","appId":"ra","theme":"midnight"},{"homePageUrl":"http://www.virginiadot.org/","errorReportEmail":"","bypassProxy":true,"signSystems":[],"links":[],"esriConfig":{"basemapSelector":{},"mapServer":{"url":"//dev-app/arcgis/rest/services/happytown/lrs/MapServer","hasLrsMeta":false,"hasNumericRouteId":false},"arcgis":{}},"id":"happytown","name":"LRS Common Scenarios","logo":"images/logos/happytown-logo.png","appId":"ra","theme":"transcend"},{"homePageUrl":"http://www.iowadot.gov/","message":"<strong>Welcome!</strong> If this is your first time viewing Road Analyzer check out our <a href=\"docs/#/user\" target=\"_blank\">Users Guide</a> for help navigating the application.","lrsProfileId":9,"errorReportEmail":"","bypassProxy":true,"signSystems":[],"links":[],"esriConfig":{"basemapSelector":{},"mapServer":{"url":"//dev-app/arcgis/rest/services/iadot/lrs/MapServer","hasLrsMeta":false,"hasNumericRouteId":false},"arcgis":{}},"id":"iadot","name":"IOWA DOT Road Analyzer","logo":"images/logos/iadot-logo.png","appId":"ra","theme":"khaki"},{"homePageUrl":"http://www.in.gov/indot/","lrsProfileId":3,"errorReportEmail":"","bypassProxy":true,"signSystems":[],"links":[],"esriConfig":{"basemapSelector":{},"mapServer":{"url":"//dev-app/arcgis/rest/services/indot/lrs/MapServer","hasLrsMeta":false,"hasNumericRouteId":false},"arcgis":{}},"id":"indot","name":"Indiana Road Analyzer","logo":"images/logos/indot-logo.png","appId":"ra","theme":"royal"},{"homePageUrl":"http://www.michigan.gov/mdot/","errorReportEmail":"","bypassQueryOrderBy":true,"bypassProxy":true,"signSystems":[],"links":[],"esriConfig":{"basemapSelector":{},"mapServer":{"url":"//dev-app/arcgis/rest/services/mdot/lrs/MapServer","hasLrsMeta":false,"hasNumericRouteId":false},"arcgis":{}},"id":"mdot","name":"Michigan Road Analyzer","logo":"images/logos/mdot-logo.png","appId":"ra","theme":"spacelab"},{"homePageUrl":"http://www.roads.maryland.gov/Home.aspx","message":"<strong>Welcome!</strong> If this is your first time viewing Road Analyzer check out our <a href=\"docs/#/user\" target=\"_blank\">Users Guide</a> for help navigating the application.","errorReportEmail":"","bypassProxy":true,"signSystems":[],"links":[],"esriConfig":{"basemapSelector":{},"mapServer":{"url":"//dev-app/arcgis/rest/services/mdsha/lrs/MapServer","hasLrsMeta":false,"hasNumericRouteId":false},"arcgis":{}},"id":"mdsha","name":"MDSHA Road Analyzer","logo":"images/logos/mdsha-logo.png","appId":"ra","theme":"superhero"},{"homePageUrl":"http://www.ncdot.gov/","message":"<strong>Welcome!</strong> If this is your first time viewing Road Analyzer check out our <a href=\"docs/#/user\" target=\"_blank\">Users Guide</a> for help navigating the application.","lrsProfileId":7,"errorReportEmail":"","bypassProxy":true,"signSystems":[],"links":[],"esriConfig":{"basemapSelector":{},"mapServer":{"url":"//dev-app/arcgis/rest/services/ncdot/lrs/MapServer","hasLrsMeta":false,"hasNumericRouteId":false},"arcgis":{}},"id":"ncdot","name":"North Carolina Road Analyzer","logo":"images/logos/ncdot-logo.png","appId":"ra","theme":"ocean"},{"homePageUrl":"http://www.dot.state.oh.us/pages/home.aspx","lrsProfileId":4,"errorReportEmail":"","bypassProxy":true,"signSystems":[],"links":[],"esriConfig":{"basemapSelector":{"map":"topo"},"mapServer":{"url":"//dev-app/arcgis/rest/services/odot/lrs/MapServer","hasLrsMeta":false,"hasNumericRouteId":false},"arcgis":{}},"id":"odot","name":"Ohio Road Analyzer","logo":"images/logos/odot-logo.png","appId":"ra","theme":"cartographic"},{"homePageUrl":"http://www.penndot.gov/","message":"<strong>Welcome!</strong> If this is your first time viewing Road Analyzer check out our <a href=\"docs/#/user\" target=\"_blank\">Users Guide</a> for help navigating the application.","lrsProfileId":8,"errorReportEmail":"","bypassProxy":true,"signSystems":[],"links":[],"settings":{"id":3,"scale":50.0,"setLoadedProfileAsDefault":true,"visibility":{"versionSelection":true,"dateSelection":true}},"esriConfig":{"basemapSelector":{},"mapServer":{"url":"//dev-app/arcgis/rest/services/penndot/lrs/MapServer","hasLrsMeta":true,"hasNumericRouteId":true},"arcgis":{}},"id":"penndot","name":"PennDOT Road Analyzer","logo":"images/logos/penndot-logo.png","appId":"ra","theme":"ocean"},{"homePageUrl":"http://www.transcendspatial.com/?ref=road-analyzer","message":"<strong>Welcome!</strong> If this is your first time viewing Road Analyzer check out our <a href=\"docs/#/user\" target=\"_blank\">Users Guide</a> for help navigating the application.","lrsProfileId":1,"errorReportEmail":"","routeListOrderBy":"10-11","bypassProxy":true,"signSystems":[],"links":[],"esriConfig":{"basemapSelector":{},"mapServer":{"url":"//dev-app/arcgis/rest/services/tss/lrs/MapServer","hasLrsMeta":false,"hasNumericRouteId":false},"arcgis":{}},"id":"tss","name":"Road Analyzer","logo":"images/logos/tss-logo.png","appId":"ra","theme":"transcend"},{"homePageUrl":"http://www.txdot.gov/","lrsProfileId":6,"errorReportEmail":"","bypassProxy":false,"signSystems":[],"links":[],"settings":{"id":2,"setLoadedProfileAsDefault":true,"maxQueryLayers":10,"visibility":{"versionSelection":false,"dateSelection":false}},"esriConfig":{"basemapSelector":{},"mapServer":{"url":"//dev-app/arcgis/rest/services/txdot/lrs/MapServer","hasLrsMeta":false,"hasNumericRouteId":false},"arcgis":{}},"id":"txdot","name":"TXDOT Road Analyzer","logo":"images/logos/txdot-logo.png","appId":"ra","theme":"patriot"},{"homePageUrl":"http://www.virginiadot.org/","lrsProfileId":5,"errorReportEmail":"","bypassProxy":true,"signSystems":[],"links":[],"settings":{"id":1,"setLoadedProfileAsDefault":true,"maxQueryLayers":15,"visibility":{"versionSelection":false,"dateSelection":false}},"esriConfig":{"basemapSelector":{},"mapServer":{"url":"//dev-app/arcgis/rest/services/vdot/lrs/MapServer","hasLrsMeta":false,"hasNumericRouteId":true},"arcgis":{}},"id":"vdot","name":"Straight Line Diagram","logo":"images/logos/vdot-logo.png","appId":"ra","theme":"contrast"},{"homePageUrl":"http://www.transportation.wv.gov/Pages/default.aspx","message":"<strong>Attention!</strong> A new roadway network is being implemented, until the process is finished the data driving this application will be incomplete.","lrsProfileId":2,"errorReportEmail":"","routeListOrderBy":"10-11","bypassProxy":true,"signSystems":[],"links":[],"esriConfig":{"basemapSelector":{},"mapServer":{"url":"//dev-app/arcgis/rest/services/wvdot/lrs/MapServer","hasLrsMeta":false,"hasNumericRouteId":false},"arcgis":{}},"id":"wvdot","name":"West Virginia Road Analyzer","logo":"images/logos/wvdot-logo.png","appId":"ra","theme":"sunrise"}]

   // Mock our HTTP calls.
   $httpBackend.whenGET(/api\/my-app\/profile/)
    .respond(angular.copy(mocks));
 })
 </file>
 </example>
 */
/**
 * @ngdoc service
 * @name transcend.app.service:AppProfile
 *
 * @description
 The 'AppProfile' factory creates and returns a profile object containing configuration options

 * @requires $resource
 * @requires transcend.app.appConfig
 *
 *
 */
/**
 * @ngdoc controller
 * @name transcend.app.controller:ReportsDialogCtrl
 *
 * @description
 * The 'ReportsDialogCtrl' controller controls the dialog window and ensures the correct profile information is set.
 *
 * @requires $scope
 * @requires transcend.app.appConfig
 * @requires $modalInstance
 *
 */
/**
 * @ngdoc directive
 * @name transcend.app.directive:reportsMenu
 *
 * @restrict EA
 * @scope
 *
 * @description
 * The 'reportsMenu' directive creates a simple navigation menu item to show the "reports list" component.
 *
 * <pre>
 <li class="dropdown" dropdown>
 <a href class="dropdown-toggle" dropdown-toggle>More <b class="caret"></b></a>
 <ul class="dropdown-menu" role="menu">
 <li reports-menu></li>
 </ul>
 </li>
 * </pre>
 *
 * @requires transcend.app.service:$showModal
 * @requires transcend.app.appConfig
 *
 * @example
 <example module="app">
 <file name="index.html">
 <div ng-controller="Ctrl as ctrl">
 <nav class="navbar navbar-default" role="navigation">
 <span class="navbar-brand" >Nav Bar</span>
 <ul class="nav navbar-nav main-nav nav-pills">
 <li class="dropdown" dropdown>
 <a href class="dropdown-toggle" dropdown-toggle><i class="glyphicon glyphicon-plus"></i> More <b class="caret"></b></a>
 <ul class="dropdown-menu" role="menu">
 <li><a href role="menuitem">Menu Item 1</a></li>
 <li reports-menu></li>
 <li><a href role="menuitem">Menu Item 1</a></li>
 </ul>
 </li>
 </ul>
 </nav>

 </div>
 </file>
 <file name="script.js">
 angular.module('app', ['transcend.app', 'ngMockE2E'])
 .controller('Ctrl', function() {
  });
 </file>
 <file name="mocks.js">
 angular.module('app')
 .run(function($httpBackend, $roleManager, appConfig){
   $roleManager.enabled = false;

   var mocks = [{"name":"Straight Line Diagram","fileName":"ra","description":"Straight Line Diagram (SLD) report snapshot.","category":"","app":"","parameters":[{"name":"FromMeasure","type":"Double","value":0.0},{"name":"ToMeasure","type":"Double","value":0.0},{"name":"RepeatingImage","type":"Image"},{"name":"SignImage","type":"Image"},{"name":"RouteId","type":"String","value":""},{"name":"BarLegendImage","type":"Image"},{"name":"StickLegendImage","type":"Image"},{"name":"PageWidth","type":"Double","value":11.0},{"name":"PageHeight","type":"Double","value":8.5},{"name":"Grayscale","type":"Boolean","value":false},{"name":"Scale","type":"Double","value":0.0}]}];

   // Mock our HTTP calls.
   $httpBackend.whenGET('/api/report?category=' + appConfig.profile.id)
    .respond(angular.copy(mocks));
 })
 </file>
 </example>
 **/
/**
 * @ngdoc controller
 * @name transcend.app.controller:AccessControlMgrDialogCtrl

 *
 * @description
 * The 'AccessControlMgrDialogCtrl' controller holds the logic responsible for handling roles and permissions within the
 * Access Control Menu.
 *
 * @requires $scope
 * @requires $modalInstance
 * @requires transcend.security.$roleManager
 * @requires securityManagerConfig
 *
 */
/**
 * @ngdoc directive
 * @name transcend.app.directive:accessControlManagerMenu
 *
 * @restrict EA
 * @scope
 *
 * @description
 * The 'accessControlManagerMenu' directive creates a simple navigation menu item to show the "access control manager"
 * component.
 *
 * <pre>
 <li class="dropdown" dropdown>
 <a href class="dropdown-toggle" dropdown-toggle>More <b class="caret"></b></a>
   <ul class="dropdown-menu" role="menu">
    <li access-control-manager-menu></li>
   </ul>
 </li>
 * </pre>
 *
 * @requires transcend.app.service:$showModal
 * @requires transcend.app.appConfig
 * @example
 <example module="app">
 <file name="index.html">
 <div ng-controller="Ctrl as ctrl">
 <nav class="navbar navbar-default" role="navigation">
 <span class="navbar-brand" >Nav Bar</span>
 <ul class="nav navbar-nav main-nav nav-pills">
 <li class="dropdown" dropdown>
 <a href class="dropdown-toggle" dropdown-toggle><i class="glyphicon glyphicon-plus"></i> More <b class="caret"></b></a>
 <ul class="dropdown-menu" role="menu">
 <li><a href role="menuitem">Menu Item 1</a></li>
 <li access-control-manager-menu></li>
 <li><a href role="menuitem">Menu Item 1</a></li>
 </ul>
 </li>
 </ul>
 </nav>
 </div>
 </file>
 <file name="script.js">
 angular.module('app', ['transcend.app', 'ngMockE2E'])
 .controller('Ctrl', function() {
  });
 </file>
 <file name="mocks.js">
 angular.module('app')
 .run(function($httpBackend, $roleManager){
   $roleManager.enabled = false;

   var mocks = [{id: 'DefaultConnection', name: 'Default Connection', provider: 'System.Data.SqlServerCe.4.0', connectionString:'Data Source=|DataDirectory|\TDS.sdf'},
              {id: 'MySqlDb', name: 'Sql Server DB', provider: 'System.Data.SqlClient', connectionString:'data source=sql.myhost.com, 1433;initial catalog=MY_CATALOG;persist security info=True;user id=suer;password=password'},
              {id: 'MyOracleDb', name: 'Oracle DB', provider: 'System.Data.OracleClient', connectionString:'Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=oracle.myhost.com)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=MY_SERVICE)));User Id=user;Password=password;'}];

   // Mock our HTTP calls.
   $httpBackend.whenGET('/api/data/source')
    .respond(angular.copy(mocks));

   angular.forEach(mocks, function(mock){
     $httpBackend.whenGET('/api/data/source/' + mock.id)
      .respond(angular.copy(mock));

     $httpBackend.whenPOST('/api/data/source/test/' + mock.id)
      .respond({"passed":true,"message":"Connection was successful","provider":mock.provider});
   });
 })
 </file>
 </example>
 */
/**
 * @ngdoc directive
 * @name transcend.app.directive:inlineAccountMgr
 *
 * @restrict EA
 * @scope
 *
 * @description
 * The 'inline account manager' directive creates a simple wrapper for the
 * {@link transcend.security.directive:inlineUserSignIn Inline User Sign In Directive}, which has all of the necessary
 * hooks and actions to support managing a user's account - including changing password, forgot password, update
 * info, etc.
 *
 * @requires transcend.app.service:$showModal
 * @example
 <example module="app">
 <file name="index.html">
 <div ng-controller="Ctrl">
 <nav class="navbar navbar-default" role="navigation">
 <inline-account-mgr on-success="onSuccess(user, manual)" on-fail="onFail()"></inline-account-mgr>
 </nav>
 Enter a user name of: <strong>rgreen</strong><br/>
 Enter a password of: <strong>abc123</strong>
 <hr/>
 <div ng-style="style">{{message}}</div>
 </div>
 </file>
 <file name="script.js">
 angular.module('app', ['transcend.app', 'ngMockE2E'])
 .run(function($roleManager, $authenticate){
  $authenticate.rememberMe = false;
  $roleManager.enabled = false;
 })

 .controller('Ctrl', function($scope, $authenticate) {
    $scope.onSuccess = function(user, manual) {
      $scope.message = 'Congratulations ' + user.firstName + '! You have successfully signed in.';
      $scope.style = {color: 'green'};
    };

    $scope.onFail = function() {
      $scope.message = 'Sorry! Your credentials were not valid.';
      $scope.style = {color: 'red'};
    };
});
 </file>
 <file name="mocks.js">
 angular.module('app')
 .run(function($httpBackend){
   // Mock our HTTP calls.
   $httpBackend.whenPOST('/token', 'grant_type=password&username=rgreen&password=abc123')
    .respond({"access_token":"test-token","token_type":"bearer","expires_in":7775999,"id":"b6aad2d8-2fdd-4b07-91f9-ada394f1ee95","userName":"rgreen","roles":"","email":"","firstName":"Rusty","lastName":"Green",".issued":"Mon, 17 Aug 2015 17:03:31 GMT",".expires":"Sun, 15 Nov 2015 17:03:31 GMT"});

   $httpBackend.whenPOST('/token', /^(?!.*grant_type=password&username=rgreen&password=abc123)/)
    .respond(400, {"error":"invalid_grant","error_description":"The user name ('siteadmin') or password is incorrect."});

   $httpBackend.whenPOST('/api/account/signout')
    .respond();

   $httpBackend.whenPOST('/api/account/forgotpassword')
    .respond(function(method,url,data,header){
      console.log('Recieved data: ', method,url,data,header);
    });

   $httpBackend.whenPOST('/api/account/resetpassword')
    .respond(function(method,url,data,header){
      console.log('Recieved data: ', method,url,data,header);
    });
 })
 </file>
 </example>
 */
/**
 * @ngdoc controller
 * @name transcend.app.controller:SignInRequiredMsgCtrl

 *
 * @description
 * The 'SignInRequiredMsgCtrl' controller holds the logic responsible for notifying the user if a sign up is required.
 * Access Control Menu.
 *
 * @requires $scope
 * @requires $modalInstance
 * @requires transcend.security.$roleManager
 *
 */
